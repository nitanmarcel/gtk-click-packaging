#!/bin/bash

set -e

CLICK_ARCH=$(dpkg-architecture -qDEB_HOST_ARCH)
CLICK_FRAMEWORK=ubuntu-sdk-16.04.5

dlarch="$1"

pkgver=1.65.0
srcdir=$BUILD_DIR
pkgdir=$INSTALL_DIR
tmpdir=/tmp/deb-click-packaging
tmpoutdir=$tmpdir/out
tmpbuilddir=$tmpdir/build

mkdir -p $srcdir $pkgdir $tmpdir $tmpoutdir $tmpbuilddir
cp -r $ROOT/* $tmpbuilddir

source $tmpbuilddir/gtk_click_config

aptfiles=$(cat $tmpbuilddir/apt_file | sed -e 's/.*: \///g')
pkgs=($gtk_deb_packages)

for pkg in "${pkgs[@]}"
do
	if [[ "${pkg}" == *":all" ]]; then
		dlpkgs+="${pkg} "
	else
		dlpkgs+="${pkg}":"${ARCH} "
	fi
done

# Currently ignoring all insalled depedencies and downloading them again.
cd $tmpdir

apt-get download $(apt-cache depends --recurse --no-recommends --no-suggests \
  --no-conflicts --no-breaks --no-replaces --no-enhances \
  --no-pre-depends $dlpkgs | grep "^\w")

for f in *.deb; do
    dpkg -X $f $tmpoutdir/
done

cd $tmpoutdir

for file in $aptfiles; do
    if [ -f "$file" ]; then
        cp --parents "$file" $pkgdir/
    fi
done

mv $pkgdir/usr/share/applications/$executable_id.desktop $pkgdir
sed -i 's/Exec=/Exec=env PATH="usr\/bin:usr\/sbin:usr\/lib:$PATH" LD_LIBRARY_PATH="usr\/lib:$LD_LIBRARY_PATH" $executable_id.wrapper /g' $pkgdir/$executable_id.desktop
sed -i 's/Icon=\//Icon=.\//g' $pkgdir/$executable_id.desktop
echo "X-Ubuntu-Touch=true" >> $pkgdir/$executable_id.desktop
# if mirx flag
echo "X-Ubuntu-XMir-Enable=true" >> $pkgdir/$executable_id.desktop

sed -i "s/@CLICK_ARCH@/$CLICK_ARCH/g" $tmpbuilddir/manifest.json
sed -i "s/@CLICK_FRAMEWORK@/$CLICK_FRAMEWORK/g" $tmpbuilddir/manifest.json
sed -i "s/@CLICK_DOMAIN@/$click_domain/g" "$tmpbuilddir/manifest.json"
sed -i "s/@CLICK_DESCRIPTION@/$click_description/g" "$tmpbuilddir/manifest.json"
sed -i "s/@CLICK_VERSION@/$click_version/g" "$tmpbuilddir/manifest.json"
sed -i "s/@CLICK_NAME@/$executable_name/g" "$tmpbuilddir/manifest.json"
sed -i "s/@CLICK_ID@/$executable_id/g" "$tmpbuilddir/manifest.json"
sed -i "s/@CLICK_MAINTAINER@/$click_maintainer/g" "$tmpbuilddir/manifest.json"

cp $tmpbuilddir/manifest.json $pkgdir/
cp "$tmpbuilddir/gtk_click.apparmor" $pkgdir/$executable_id.apparmor
cp "$tmpbuilddir/gtk_click.wrapper" $pkgdir/$executable_id.wrapper
cp "$tmpbuilddir/logo.svg" $pkgdir/
chmod a+x $pkgdir/$executable_id.wrapper

exit 0